﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class SoundController : MonoBehaviour {

    public AudioMixer mainAudioMixer;
    public GameObject musicButton;
    public GameObject soundFXButton;
    public Sprite musicOnSprite;
    public Sprite musicOffSprite;
    public Sprite soundFXOnSprite;
    public Sprite soundFXOffSprite;

    private bool musicMuted;
    private bool soundFXMuted;
    private Image musicButtonImage;
    private Image soundFXButtonImage;
    private AudioSource soundFXOnAudioSource;

    // Use this for initialization
    void Start () {
        musicMuted = PlayerPrefs.GetInt("MusicMuted") != 0;
        soundFXMuted = PlayerPrefs.GetInt("SoundFXMuted") != 0;
        musicButtonImage = musicButton.GetComponent<Image>();
        soundFXButtonImage = soundFXButton.GetComponent<Image>();
        soundFXOnAudioSource = soundFXButton.GetComponent<AudioSource>();

        RefreshMusicButton();
        RefreshMusicMuted();
        RefreshSoundFXButton();
        RefreshSoundFXMuted();
    }

    private void RefreshMusicMuted()
    {
        float volume = musicMuted ? -80.0f : 0.0f;
        mainAudioMixer.SetFloat("MusicVolume", volume);
    }

    private void RefreshSoundFXMuted()
    {
        float volume = soundFXMuted ? -80.0f : 0.0f;
        mainAudioMixer.SetFloat("SoundFXVolume", volume);
    }

    private void RefreshMusicButton()
    {
        musicButtonImage.sprite = musicMuted ? musicOffSprite : musicOnSprite;
    }

    private void RefreshSoundFXButton()
    {
        soundFXButtonImage.sprite = soundFXMuted ? soundFXOffSprite : soundFXOnSprite;
    }

    public void ToggleMusicMute()
    {
        musicMuted = !musicMuted;
        PlayerPrefs.SetInt("MusicMuted", musicMuted ? 1 : 0);
        RefreshMusicButton();
        RefreshMusicMuted();
    }

    public void ToggleSoundFXMute()
    {
        soundFXMuted = !soundFXMuted;
        PlayerPrefs.SetInt("SoundFXMuted", soundFXMuted ? 1 : 0);
        RefreshSoundFXButton();
        RefreshSoundFXMuted();
        soundFXOnAudioSource.Play();
    }
}
