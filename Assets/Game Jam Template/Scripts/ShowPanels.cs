﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ShowPanels : MonoBehaviour {

	public GameObject optionsPanel;							//Store a reference to the Game Object OptionsPanel 
	public GameObject optionsTint;							//Store a reference to the Game Object OptionsTint 
	public GameObject menuPanel;							//Store a reference to the Game Object MenuPanel 
	public GameObject pausePanel;							//Store a reference to the Game Object PausePanel
    public GameObject gameOverPanel;
    public GameObject soundControlPanel;
    public Text gameOverPanelScoreText;
    public Text gameOverPanelNewScoreText;


    //Call this function to activate and display the Options panel during the main menu
    public void ShowOptionsPanel()
	{
		optionsPanel.SetActive(true);
		optionsTint.SetActive(true);
	}

	//Call this function to deactivate and hide the Options panel during the main menu
	public void HideOptionsPanel()
	{
		optionsPanel.SetActive(false);
		optionsTint.SetActive(false);
	}

	//Call this function to activate and display the main menu panel during the main menu
	public void ShowMenu()
	{
        HideGameOverMenu();
        menuPanel.SetActive(true);
        soundControlPanel.SetActive(true);

        StartOptions startOptions = null;
        if (gameObject != null)
        {
            startOptions = gameObject.GetComponent<StartOptions>();
        }
        if (startOptions == null)
        {
            Debug.Log("Cannot find 'StartOptions' script");
        }
        else
        {
            startOptions.RefreshHighScore();
        }
    }

	//Call this function to deactivate and hide the main menu panel during the main menu
	public void HideMenu()
	{
		menuPanel.SetActive (false);
        soundControlPanel.SetActive(false);
    }
	
	//Call this function to activate and display the Pause panel during game play
	public void ShowPausePanel()
	{
		pausePanel.SetActive (true);
		optionsTint.SetActive(true);
	}

	//Call this function to deactivate and hide the Pause panel during game play
	public void HidePausePanel()
	{
		pausePanel.SetActive (false);
		optionsTint.SetActive(false);

	}

    //Call this function to activate and display the Game Over panel
    public void ShowGameOverMenu(int score)
    {
        HideMenu();
        gameOverPanel.SetActive(true);
        soundControlPanel.SetActive(true);
        gameOverPanelScoreText.text = "Score: " + score;
        int highscore = PlayerPrefs.GetInt("highscore");

        if (score > highscore)
        {
            PlayerPrefs.SetInt("highscore", score);
            gameOverPanelNewScoreText.enabled = true;

        }
        else
        {
            gameOverPanelNewScoreText.enabled = false;
        }
    }

    public void HideGameOverMenu()
    {
        gameOverPanel.SetActive(false);
        soundControlPanel.SetActive(false);
    }
}
