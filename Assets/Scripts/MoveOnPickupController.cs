﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveOnPickupController : MonoBehaviour {

    [HideInInspector] public bool alreadyPickedUp;
    private float totalMoveTowardsTargetTime;

    private float currentMoveTowardsTargetTime;
    private bool movingTowardsTarget;
    private Vector2 positionAtPickup;
    private Vector2 targetPosition;
    private bool changeRotation;
    private Quaternion rotationAtPickup;
    private Quaternion targetRotation;
    private OnAfterMoveOnPickupDelegate afterMoveOnPickupDelegate;

    void Start()
    {
        totalMoveTowardsTargetTime = 0.5f;
        currentMoveTowardsTargetTime = 0.0f;
        movingTowardsTarget = false;
        alreadyPickedUp = false;
    }

    void Update()
    {
        if (!movingTowardsTarget)
        {
            return;
        }

        currentMoveTowardsTargetTime += Time.deltaTime;

        float t = currentMoveTowardsTargetTime / totalMoveTowardsTargetTime;
        if (t >= 1)
        {
            movingTowardsTarget = false;
            afterMoveOnPickupDelegate();
        }
        else
        {
            t = Mathf.Sin(t * Mathf.PI * 0.5f);
            //t = 1f - Mathf.Cos(t * Mathf.PI * 0.5f);
            transform.position = Vector2.Lerp(positionAtPickup, targetPosition, t);
            if (changeRotation)
            {
                transform.rotation = Quaternion.Lerp(rotationAtPickup, targetRotation, t);
            }
        }
    }

    private void OnPickupHelper()
    {
        alreadyPickedUp = true;
        positionAtPickup = transform.position;

        ConstantUpwardMovementController movementController = (ConstantUpwardMovementController)gameObject.GetComponent(typeof(ConstantUpwardMovementController));
        if (movementController)
        {
            movementController.enabled = false;
        }

        movingTowardsTarget = true;
    }

    public delegate void OnAfterMoveOnPickupDelegate();

    public void OnPickup(Vector2 targetPosition, OnAfterMoveOnPickupDelegate afterMoveOnPickupDelegate)
    {
        if (alreadyPickedUp)
        {
            return;
        }

        this.targetPosition = targetPosition;
        this.afterMoveOnPickupDelegate = afterMoveOnPickupDelegate;

        OnPickupHelper();
    }

    public void OnPickup(Vector2 targetPosition, Quaternion targetRotation, OnAfterMoveOnPickupDelegate afterMoveOnPickupDelegate)
    {
        if (alreadyPickedUp)
        {
            return;
        }

        this.targetPosition = targetPosition;
        changeRotation = true;
        this.targetRotation = targetRotation;
        rotationAtPickup = transform.rotation;
        this.afterMoveOnPickupDelegate = afterMoveOnPickupDelegate;

        OnPickupHelper();
    }
}
