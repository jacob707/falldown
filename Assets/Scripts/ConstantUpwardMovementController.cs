﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConstantUpwardMovementController : MonoBehaviour
{
    private Rigidbody2D rb;
    private float speed = 1f;

    void Start()
    {
        rb = gameObject.GetComponent<Rigidbody2D>();
        SetSpeed(speed);
    }

    public void SetSpeed(float newSpeed)
    {
        speed = newSpeed;
        if (rb)
        {
            rb.velocity = new Vector2(0, speed);
        }
    }
}
