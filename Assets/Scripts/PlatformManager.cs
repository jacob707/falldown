﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlatformManager : MonoBehaviour {

    public GameObject platform;
    public GameObject lockedPlatform;
    public GameObject scoreBoostPickup;
    public GameObject keyPickup;
    public GameObject gameUICanvas;

    public int width = 18;
    public float speed = 3f;
    public float speedIncrement = 1f;
    public float timePerWave = 10f;

    public float platformWidth = 2f;
    public Sprite soloPlatformSprite;
    public Sprite leftEmptyPlatformSprite;
    public Sprite rightEmptyPlatformSprite;
    public Sprite emptyUIKeySprite;
    public Sprite filledUIKeySprite;

    public bool playerHasKey = false;

    private int halfWidth;
    private float halfPlatformWidth;
    private float platformHoleProbability = 0.7f;
    private float platformRotatingProbability = 0.1f;
    private int platformsPerRow;
    private bool previousRowHadRotatingPlatform;

    private float spawnTime;

    private float scoreBoostPickupProbability = 0.4f;
    private float keyPickupProbability = 0.15f;

    private int score;
    private bool gameOver;
    private Text scoreText;
    private Image keyIndicator;
    private Animator keyIndicatorAnim;
    private bool missingKeyAlarm;
    private AudioSource misingKeyAlarmSound;
    private GameObject uiGameObject;
    private AudioSource gameMusicAudioSource;
    private AudioSource gameOverSoundAudioSource;

    private float lockedPlatformRowProbabilityPlayerHasKey = .2f;
    private float lockedPlatformRowProbabilityPlayerDoesNotHaveKey = .1f;
    private int minSpawnedKeysBeforeLockedPlatformSpawn = 1;
    private int keysSpawnedSinceLastLockedPlatform;
    private GameObject activeLockedPlatform;

    void Start ()
    {
        gameOver = false;
        gameUICanvas.SetActive(true);

        uiGameObject = GameObject.FindWithTag("UI");
        // uiGameObject won't be found if runnning Game scene directly from editor
        if (uiGameObject)
        {
            ShowPanels showPanels = uiGameObject.GetComponent<ShowPanels>();
            showPanels.HideGameOverMenu();
            gameMusicAudioSource = uiGameObject.GetComponent<AudioSource>();
        }

        gameOverSoundAudioSource = GameObject.FindWithTag("GameOverSound").GetComponent<AudioSource>();

        previousRowHadRotatingPlatform = false;
        keysSpawnedSinceLastLockedPlatform = 0;
        activeLockedPlatform = null;
        score = 0;

        halfWidth = width / 2;
        halfPlatformWidth = platformWidth / 2.0f;

        platformsPerRow = (int)Mathf.Ceil(((float)width) / platformWidth);

        SetPlatformControllerSpeed();
        RefreshSpawnTime();
        
        scoreText = GameObject.Find("ScoreText").GetComponent<Text>();
        RefreshScoreText();

        GameObject keyIndicatorGameObject = GameObject.Find("KeyIndicator");
        keyIndicator = keyIndicatorGameObject.GetComponent<Image>();
        keyIndicatorAnim = keyIndicator.GetComponent<Animator>();
        misingKeyAlarmSound = keyIndicatorGameObject.GetComponent<AudioSource>();
        RefreshKeyIndicator();
        SetMissingKeyAlarm(false);

        StartCoroutine("SpawnRowEnumerator");
    }

    public void GameOver()
    {
        /*string[] highScores = (PlayerPrefs.GetString ("highscores")).Split(',');
		foreach (string word in highScores)
		{
			print("word:" + word);
		}*/
        //gameOverText.text = "Game Over!";
        if (gameOver)
        {
            return;
        }
        gameOver = true;
        scoreText.text = "" + score;
        SetMissingKeyAlarm(false);
        TurnDownGameMusic();
        Invoke("PlayGameoverSound", 0.1f);

        Invoke("AfterGameOverFcn", 1);
    }

    public void PlayGameoverSound()
    {
        gameOverSoundAudioSource.Play();
    }

    public void AfterGameOverFcn()
    {
        TurnUpGameMusic();
        gameUICanvas.SetActive(false);
        GameObject uiGameObject = GameObject.FindWithTag("UI");
        // uiGameObject won't be found if runnning Game scene directly from editor
        if (uiGameObject)
        {
            ShowPanels showPanels = uiGameObject.GetComponent<ShowPanels>();
            showPanels.ShowGameOverMenu(score);
        }
    }

    IEnumerator SpawnRowEnumerator()
    {
        int waveCount = 0;
        while (true)
        {
            SpawnRow();

            if (waveCount * spawnTime >= timePerWave)
            {
                speed += speedIncrement;
                SetPlatformControllerSpeed();
                RefreshSpawnTime();
                waveCount = 0;
            }
            else
            {
                waveCount++;
            }
            yield return new WaitForSeconds(spawnTime);
            if (gameOver)
            {
                yield break;
            }
        }
    }

    void SetPlatformControllerSpeed()
    {
        ConstantUpwardMovementController[] allMovementControllers = (ConstantUpwardMovementController[])FindObjectsOfType(typeof(ConstantUpwardMovementController));
        foreach (ConstantUpwardMovementController curMovementControllers in allMovementControllers)
        {
            curMovementControllers.SetSpeed(speed);
        }
    }

    void RefreshSpawnTime()
    {
        float baseSpeed = 2.7f;
        float baseSpawnTime = 2f;
        float ratio = speed / baseSpeed;
        spawnTime = baseSpawnTime / ratio;
    }

    void SpawnRow()
    {
        if (gameOver)
        {
            return;
        }

        float lockedPlatformRowProbability = playerHasKey ? lockedPlatformRowProbabilityPlayerHasKey : lockedPlatformRowProbabilityPlayerDoesNotHaveKey;

        bool lockedPlatformRow = (
            playerHasKey ||
            keysSpawnedSinceLastLockedPlatform >= minSpawnedKeysBeforeLockedPlatformSpawn
        ) && !activeLockedPlatform && RollBooleanDice(lockedPlatformRowProbability);

        // Populate platformIsPresent for each of the possible platform spots:
        bool[] platformIsPresent = new bool[platformsPerRow];
        int lockedRowIndex = -1;

        if (lockedPlatformRow)
        {
            lockedRowIndex = Random.Range(0, platformsPerRow - 1);
            for (int j = 0; j < platformsPerRow; j++)
            {
                platformIsPresent[j] = true;
            }
        }
        else
        {
            bool foundPresentPlatform = false;
            bool foundHole = false;

            for (int j = 0; j < platformsPerRow; j++)
            {
                platformIsPresent[j] = !RollBooleanDice(platformHoleProbability);
                if (platformIsPresent[j])
                {
                    foundPresentPlatform = true;
                }
                else
                {
                    foundHole = true;
                }

            }

            // Make sure there is at least one hole and one platform:
            if (!foundHole)
            {
                platformIsPresent[Random.Range(0, platformsPerRow - 1)] = false;
            }
            else if (!foundPresentPlatform)
            {
                platformIsPresent[Random.Range(0, platformsPerRow - 1)] = true;
            }
        }

        // Spawn the platforms:
        bool[] platformIsRotating = new bool[platformsPerRow];
        bool atLeastOnePlatformIsRotating = false;
        GameObject[] platforms = new GameObject[platformsPerRow];
        for (int j = 0; j < platformsPerRow; j++)
        {
            if (platformIsPresent[j])
            {
                float percWidth = ((float)j) / ((float)platformsPerRow);
                float x = (percWidth * ((float)width)) - halfWidth;
                x += halfPlatformWidth;

                if (lockedPlatformRow && lockedRowIndex == j)
                {
                    platforms[j] = SpawnLockedPlatform(x);
                }
                else
                {
                    bool rotating = !lockedPlatformRow && !previousRowHadRotatingPlatform && RollBooleanDice(platformRotatingProbability);
                    platforms[j] = SpawnPlatform(x, rotating);
                    if (rotating)
                    {
                        platformIsRotating[j] = true;
                        atLeastOnePlatformIsRotating = true;
                    }
                }
            }
        }

        // Set previousRowHadRotatingPlatform so that 2 rows in a row don't have rotating platforms, because that can create situations where it's hard/impossible to get through:
        previousRowHadRotatingPlatform = atLeastOnePlatformIsRotating;

        // Set proper sprites if platform is an edge:
        for (int j = 0; j < platformsPerRow; j++)
        {
            GameObject curPlatform = platforms[j];
            if (curPlatform)
            {
                bool leftEmpty = false;
                bool rightEmpty = false;

                if (platformIsRotating[j])
                {
                    leftEmpty = true;
                    rightEmpty = true;
                }
                else
                {
                    if (j > 0 && (platformIsRotating[j - 1] || !platformIsPresent[j - 1] || j - 1 == lockedRowIndex))
                    {
                        leftEmpty = true;
                    }
                    if (j < platformsPerRow - 1 && (platformIsRotating[j + 1] || !platformIsPresent[j + 1] || j + 1 == lockedRowIndex))
                    {
                        rightEmpty = true;
                    }
                }

                SpriteRenderer spriteRenderer = (SpriteRenderer)curPlatform.GetComponent(typeof(SpriteRenderer));
                if (leftEmpty && rightEmpty)
                {
                    spriteRenderer.sprite = soloPlatformSprite;
                }
                else if (leftEmpty)
                {
                    spriteRenderer.sprite = leftEmptyPlatformSprite;
                }
                else if (rightEmpty)
                {
                    spriteRenderer.sprite = rightEmptyPlatformSprite;
                }
            }
        }

        if (!lockedPlatformRow)
        {
            if (!playerHasKey && RollBooleanDice(keyPickupProbability))
            {
                // Make sure there isn't already a key spawned:
                GameObject[] keyPickups = GameObject.FindGameObjectsWithTag("KeyPickup");
                if (keyPickups.Length == 0)
                {
                    SpawnKeyPickup();
                }
            }
            else if (RollBooleanDice(scoreBoostPickupProbability))
            {
                SpawnScoreBoostPickup();
            }
        }
    }

    GameObject SpawnPlatform(float x, bool rotating)
    {
        GameObject curPlatform = Instantiate(platform, new Vector3(x, -16), Quaternion.identity);

        ConstantUpwardMovementController movementController = (ConstantUpwardMovementController)curPlatform.GetComponent(typeof(ConstantUpwardMovementController));
        movementController.SetSpeed(speed);

        if (rotating)
        {
            PlatformController platformController = (PlatformController)curPlatform.GetComponent(typeof(PlatformController));
            platformController.rotating = true;
            int rotateDirectionMultiplier = Random.Range(0, 2) * 2 - 1; //-1 or 1
            platformController.rotationSpeed = rotateDirectionMultiplier * Random.Range(20, 130);
        }

        return curPlatform;
    }

    GameObject SpawnLockedPlatform(float x)
    {
        activeLockedPlatform = Instantiate(lockedPlatform, new Vector3(x, -16), Quaternion.identity);
 
        ConstantUpwardMovementController movementController = (ConstantUpwardMovementController)activeLockedPlatform.GetComponent(typeof(ConstantUpwardMovementController));
        movementController.SetSpeed(speed);

        keysSpawnedSinceLastLockedPlatform = 0;

        if (playerHasKey)
        {
            DeactivateLockedPlatformSpriteCollider();
        }
        else
        {
            SetMissingKeyAlarm(true);
        }

        return activeLockedPlatform;
    }

    void DeactivateLockedPlatformSpriteCollider()
    {
        if (activeLockedPlatform)
        {
            GameObject lockedPlatformSprite = activeLockedPlatform.transform.Find("LockedPlatformSprite").gameObject;
            lockedPlatformSprite.GetComponent<Collider2D>().isTrigger = true;
        }
    }

    void SpawnScoreBoostPickup()
    {
        if (gameOver)
        {
            return;
        }

        float maxX = (float)halfWidth - 1;
        float x = Random.Range(-maxX, maxX);

        GameObject curScoreBoostPickup = Instantiate(scoreBoostPickup, new Vector3(x, -14.8f), Quaternion.identity);

        ConstantUpwardMovementController movementController = (ConstantUpwardMovementController)curScoreBoostPickup.GetComponent(typeof(ConstantUpwardMovementController));
        movementController.SetSpeed(speed);
    }

    void SpawnKeyPickup()
    {
        if (gameOver)
        {
            return;
        }
        float maxX = (float)halfWidth - 1;
        float x = Random.Range(-maxX, maxX);

        GameObject curScoreBoostPickup = Instantiate(keyPickup, new Vector3(x, -14.8f), Quaternion.identity);

        ConstantUpwardMovementController movementController = (ConstantUpwardMovementController)curScoreBoostPickup.GetComponent(typeof(ConstantUpwardMovementController));
        movementController.SetSpeed(speed);

        keysSpawnedSinceLastLockedPlatform += 1;
    }

    // Update is called once per frame
    void Update () {

    }

    void RefreshScoreText()
    {
        if (gameOver)
        {
            return;
        }
        scoreText.text = "" + score;
    }

    void RefreshKeyIndicator()
    {
        keyIndicator.sprite = playerHasKey ? filledUIKeySprite : emptyUIKeySprite;
    }

    bool RollBooleanDice(float prob)
    {
        return (Random.Range(0.0f, 1.0f) < prob);
    }

    void SetMissingKeyAlarm(bool turnOn)
    {
        keyIndicatorAnim.SetBool("MissingKey", turnOn);

        if (turnOn)
        {
            misingKeyAlarmSound.Play();
            TurnDownGameMusic();
        }
        else
        {
            TurnUpGameMusic();
            misingKeyAlarmSound.Stop();
        }
    }

    void TurnDownGameMusic()
    {
        if (gameMusicAudioSource)
        {
            gameMusicAudioSource.volume = 0.4f;
        }
    }

    void TurnUpGameMusic()
    {
        if (gameMusicAudioSource)
        {
            gameMusicAudioSource.volume = 1f;
        }
    }

    public void onLockedPlatformUnlock()
    {
        if (gameOver)
        {
            return;
        }
        activeLockedPlatform = null;
        playerHasKey = false;
        RefreshKeyIndicator();
    }

    public void onPlatformCeilingPass()
    {
        if (gameOver)
        {
            return;
        }
        score += 10;
        RefreshScoreText();
    }

    public void onScoreBoostPickup()
    {
        if (gameOver)
        {
            return;
        }
        score += 100;
        RefreshScoreText();
    }

    public void onKeyPickup()
    {
        if (gameOver)
        {
            return;
        }
        playerHasKey = true;
        RefreshKeyIndicator();

        DeactivateLockedPlatformSpriteCollider();
        SetMissingKeyAlarm(false);
    }

    public void onPlayerCeilingPass()
    {
        GameOver();
    }
}
