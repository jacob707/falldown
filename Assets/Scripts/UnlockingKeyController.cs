﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnlockingKeyController : MonoBehaviour {

    private float currentMoveTowardsTargetTime;
    private bool movingTowardsTarget;
    private Vector2 startPosition;
    private GameObject targetObject;
    private float totalMoveTowardsTargetTime;
    private OnArrivalAtTargetDelegate onArrivalAtTargetDelegate;

    void Start () {
        currentMoveTowardsTargetTime = 0.0f;
        totalMoveTowardsTargetTime = 0.25f;
    }

    void Update()
    {
        if (!movingTowardsTarget)
        {
            return;
        }

        currentMoveTowardsTargetTime += Time.deltaTime;

        float t = currentMoveTowardsTargetTime / totalMoveTowardsTargetTime;
        if (t >= 1)
        {
            movingTowardsTarget = false;
            if (onArrivalAtTargetDelegate != null)
            {
                onArrivalAtTargetDelegate(gameObject);
            }
        }
        else
        {
            //t = Mathf.Sin(t * Mathf.PI * 0.5f);
            //t = 1f - Mathf.Cos(t * Mathf.PI * 0.5f);
            transform.position = Vector2.Lerp(startPosition, targetObject.transform.position, t);
            //float scale = Mathf.Clamp(1.0f - t, 0.0f, 1.0f);
            //transform.localScale = new Vector3(scale, scale, scale);
        }
    }

    public delegate void OnArrivalAtTargetDelegate(GameObject arrivingObject);

    public void StartUnlockingAnimation(GameObject targetObject, OnArrivalAtTargetDelegate onArrivalAtTargetDelegate = null)
    {
        this.targetObject = targetObject;
        this.onArrivalAtTargetDelegate = onArrivalAtTargetDelegate;
        startPosition = transform.position;
        movingTowardsTarget = true;
        currentMoveTowardsTargetTime = 0.0f;
    }
}
