﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreBoostPickupController : MonoBehaviour {

    private PlatformManager platformManager;
    private AudioSource pickupSound;

    void Start()
    {
        platformManager = GameObject.Find("PlatformManager").GetComponent<PlatformManager>();
        pickupSound = GetComponent<AudioSource>();
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Ceiling"))
        {
            Destroy(gameObject);
        }
    }

    public void AfterMoveOnPickup()
    {
        platformManager.onScoreBoostPickup();

        // Hide gameObject, then destroy it after a small delay so that audio clip has time to finish.
        // Without the delay, the audio clip might get cut off.
        Utils.MoveGameObjectOffScreen(gameObject);
        Utils.DestroyGameObjectAfterDelay(this, gameObject, 1.0f);
    }

    public void OnPickup()
    {
        MoveOnPickupController moveOnPickupController = (MoveOnPickupController)gameObject.GetComponent(typeof(MoveOnPickupController));
        if (!moveOnPickupController)
        {
            return;
        }

        Vector2 targetPosition = new Vector2(-7.0f, 14.5f);

        MoveOnPickupController.OnAfterMoveOnPickupDelegate afterMoveOnPickupDelegate = AfterMoveOnPickup;

        moveOnPickupController.OnPickup(targetPosition, afterMoveOnPickupDelegate);

        pickupSound.Play();
    }
}
