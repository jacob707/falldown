﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyPickupController : MonoBehaviour {

    private PlatformManager platformManager;
    private AudioSource pickupSound;

    void Start()
    {
        platformManager = GameObject.Find("PlatformManager").GetComponent<PlatformManager>();
        pickupSound = GetComponent<AudioSource>();
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Ceiling"))
        {
            Destroy(gameObject);
        }
    }

    private void AfterMoveOnPickup()
    {
        platformManager.onKeyPickup();
        DestroyAllOtherKeys();

        // Hide gameObject, then destroy it after a small delay so that audio clip has time to finish.
        // Without the delay, the audio clip might get cut off.
        Utils.MoveGameObjectOffScreen(gameObject);
        Utils.DestroyGameObjectAfterDelay(this, gameObject, 1.0f);
    }

    private void DestroyAllOtherKeys()
    {
        GameObject[] keyPickups = GameObject.FindGameObjectsWithTag("KeyPickup");
        foreach (GameObject keyPickup in keyPickups)
        {
            if (keyPickup != gameObject)
            {
                Destroy(keyPickup);
            }
        }
    }

    public void OnPickup()
    {
        MoveOnPickupController moveOnPickupController = (MoveOnPickupController)gameObject.GetComponent(typeof(MoveOnPickupController));
        if (!moveOnPickupController)
        {
            return;
        }

        // Start the move animation:
        Vector2 targetPosition = new Vector2(7.7f, 15.1f);
        Quaternion targetRotation = Quaternion.Euler(0, 0, 45);
        MoveOnPickupController.OnAfterMoveOnPickupDelegate afterMoveOnPickupDelegate = AfterMoveOnPickup;
        moveOnPickupController.OnPickup(targetPosition, targetRotation, afterMoveOnPickupDelegate);

        pickupSound.Play();
    }
}
