﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Utils : MonoBehaviour {

    public static void MoveGameObjectOffScreen(GameObject gameObjectToMove)
    {
        gameObjectToMove.transform.position = new Vector3(25.0f, 25.0f, 25.0f);
    }

	public static void DestroyGameObjectAfterDelay(MonoBehaviour objJustToStartCoroutine, GameObject objToDestroy, float delay = 1.0f)
    {
        objJustToStartCoroutine.StartCoroutine(DestroyGameObject(delay, objToDestroy));
    }

    static IEnumerator DestroyGameObject(float delay, GameObject objToDestroy)
    {
        yield return new WaitForSeconds(delay);
        Destroy(objToDestroy);
    }
}
