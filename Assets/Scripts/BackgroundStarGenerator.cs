﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundStarGenerator : MonoBehaviour {
    public GameObject backgroundStar;

    public GameObject ps;

    private ParticleSystem.Particle[] points;

    private ParticleSystem _particleSystem;

    // Use this for initialization
    void Start () {
        GenerateBackgroundStars();
    }

    void GenerateBackgroundStars()
    {
        /*return;
        int maxStars = 100;
        float maxX = (float)halfWidth;
        float maxY = 16.0f;

        points = new ParticleSystem.Particle[maxStars];

        for (int i = 0; i < maxStars; i++)
        {
            float x = Random.Range(-maxX, maxX);
            float y = Random.Range(-maxY, maxY);
            points[i].position = new Vector3(x, y, 1);
            points[i].startSize = 1f;
            points[i].startColor = new Color(1, 1, 1, 1);
        }

        _particleSystem = ps.GetComponent<ParticleSystem>();

        _particleSystem.SetParticles(points, points.Length);
        */
        float maxX = 9.0f;
        float maxY = 16.0f;

        for (int j = 0; j < 50; j++)
        {
            float x = Random.Range(-maxX, maxX);
            float y = Random.Range(-maxY, maxY);

            GameObject curBackgroundStar = Instantiate(backgroundStar, new Vector3(x, y), Quaternion.identity);

            Animator anim = (Animator)curBackgroundStar.GetComponent(typeof(Animator));
            anim.speed = Random.Range(0.5f, 1.5f);

            float baseScale = curBackgroundStar.transform.localScale.x;
            float scaleIncrementRange = 0.1f;
            float newScale = baseScale + Random.Range(-scaleIncrementRange, scaleIncrementRange);
            curBackgroundStar.transform.localScale = new Vector2(newScale, newScale);
        }
    }
}
