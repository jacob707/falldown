﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    public Rigidbody2D rb;
    public float speed;
    //public float maxVelocity;

    //private float sqrMaxVelocity;
    private PlatformManager platformManager;

    void Start () {
        rb = GetComponent<Rigidbody2D>();
        platformManager = GameObject.Find("PlatformManager").GetComponent<PlatformManager>();
        //sqrMaxVelocity = maxVelocity * maxVelocity;
    }
	
	void FixedUpdate () {
        float moveHorizontal = 0.0f;

#if UNITY_IOS || UNITY_ANDROID || UNITY_WP8 || UNITY_IPHONE  //Check if we are running on iOS, Android, Windows Phone 8 or Unity iPhone

        TouchPhase phase = TouchPhase.Canceled;
        Vector2 position = new Vector2();

        if (Input.GetMouseButton(0))
        {
            phase = TouchPhase.Stationary;
            position = Input.mousePosition;
        }
        else if (Input.touchCount > 0)
        {
            phase = Input.GetTouch(0).phase;
            position = Input.GetTouch(0).position;
        }

        if (phase == TouchPhase.Began || phase == TouchPhase.Stationary || phase == TouchPhase.Moved)
        {
            float halfWidth = Screen.currentResolution.width / 2.0f;
            if (position.x < halfWidth)
            {
                moveHorizontal = -1.0f;
            }
            else
            {
                moveHorizontal = 1.0f;
            }
        }
#else
        moveHorizontal = Input.GetAxis("Horizontal");
#endif //End of mobile platform dependendent compilation section started above with #elif

        if (moveHorizontal != 0.0f)
        {
            rb.AddForce(Vector2.right * (speed * moveHorizontal));
        }

        //var v = rb.velocity;
        // Clamp the velocity, if necessary
        // Use sqrMagnitude instead of magnitude for performance reasons.
        //if (v.sqrMagnitude > sqrMaxVelocity)
        //{ // Equivalent to: rigidbody.velocity.magnitude > maxVelocity, but faster.
        // Vector3.normalized returns this vector with a magnitude 
        // of 1. This ensures that we're not messing with the 
        // direction of the vector, only its magnitude.
        //rb.velocity = v.normalized * maxVelocity;
        //}
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("ScoreBoostPickup"))
        {
            ScoreBoostPickupController scoreBoostPickupController = (ScoreBoostPickupController)other.gameObject.GetComponent(typeof(ScoreBoostPickupController));
            scoreBoostPickupController.OnPickup();
        }
        else if (other.gameObject.CompareTag("KeyPickup"))
        {
            KeyPickupController keyPickupController = (KeyPickupController)other.gameObject.GetComponent(typeof(KeyPickupController));
            keyPickupController.OnPickup();
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Ceiling"))
        {
            float ceillingYPosition = other.gameObject.transform.position.y;
            float playerYPosition = gameObject.transform.position.y;

            if (playerYPosition > ceillingYPosition)
            {
                platformManager.onPlayerCeilingPass();
                Destroy(gameObject);
            }
        }
    }

    void OnTriggerStay2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("LockedPlatformUnlockArea"))
        {
            if (platformManager.playerHasKey)
            {
                GameObject lockedPlatform = other.gameObject.transform.parent.gameObject; //ugly
                LockedPlatformController lockedPlatformController = (LockedPlatformController)lockedPlatform.GetComponent(typeof(LockedPlatformController));
                lockedPlatformController.OnPlayerCollision();
            }
                
        }
    }
}
