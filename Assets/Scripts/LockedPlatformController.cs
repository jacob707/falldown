﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LockedPlatformController : MonoBehaviour {

    public GameObject unlockingKey;

    private Animator anim;
    private PlatformManager platformManager;
    private AudioSource unlockingSound;

    private int dieHash = Animator.StringToHash("Die");

    void Start()
    {
        anim = GetComponent<Animator>();
        platformManager = GameObject.Find("PlatformManager").GetComponent<PlatformManager>();
        unlockingSound = GetComponent<AudioSource>();
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Ceiling"))
        {
            Destroy(gameObject);
        }
    }

    private void SpawnUnlockingKey()
    {
        GameObject curUnlockingKey = Instantiate(unlockingKey, new Vector2(7.7f, 15.1f), Quaternion.identity);
        UnlockingKeyController unlockingKeyController = (UnlockingKeyController)curUnlockingKey.GetComponent(typeof(UnlockingKeyController));

        UnlockingKeyController.OnArrivalAtTargetDelegate onArrivalAtTargetDelegate = OnUnlockingKeyArrival;
        unlockingKeyController.StartUnlockingAnimation(gameObject, onArrivalAtTargetDelegate);
    }

    private void DestroyGameObject()
    {
        Destroy(gameObject);
    }

    public void OnUnlockingKeyArrival(GameObject unlockingKey)
    {
        Destroy(unlockingKey);
        //anim.SetTrigger(dieHash);
    }

    public void OnPlayerCollision()
    {
        if (platformManager.playerHasKey)
        {
            platformManager.onLockedPlatformUnlock();
            anim.SetTrigger(dieHash);
            SpawnUnlockingKey();
            unlockingSound.Play();
        }
    }

    public void OnDyingAnimationEnd()
    {
        // Hide gameObject, then destroy it after a small delay so that audio clip has time to finish.
        // Without the delay, the audio clip might get cut off.
        Utils.MoveGameObjectOffScreen(gameObject);
        Utils.DestroyGameObjectAfterDelay(this, gameObject, 1.0f);
    }
}
