﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformController : MonoBehaviour {

    public int rotationSpeed = 0;
    public bool rotating = false;
    private PlatformManager platformManager;

    void Start () {
        platformManager = GameObject.Find("PlatformManager").GetComponent<PlatformManager>();
    }

    void Update()
    {
        //float translation = Time.deltaTime * speed;
        //rb.transform.Translate(0, translation, 0, Space.World);
        if (rotating)
        {
            transform.Rotate(new Vector3(0, 0, rotationSpeed) * Time.deltaTime);
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Ceiling"))
        {
            platformManager.onPlatformCeilingPass();
            Destroy(gameObject);
        }
    }
}
